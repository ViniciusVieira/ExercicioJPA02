package br.ucsal.bes20182.bd2.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(name = "cnpj", columnDefinition = "char(11)")
	String cnpj;

	@Column(name = "nome", length = 40, nullable = false)
	String nome;

	@ManyToMany
	@JoinTable(name="tab_pessoa_juridica_ramo_atividade")
	List<RamoAtividade> ramosAtividade;

	@Column(name = "faturamento", columnDefinition = "numeric(10,2)", nullable = false)
	Double faturamento;

	@ManyToMany(mappedBy="clientes")
	List<Vendedor> vendedores;
	
	
	
	public PessoaJuridica() {
		// TODO Auto-generated constructor stub
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}
	
	
	
	
	
	
	
	
}
